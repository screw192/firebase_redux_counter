import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {add, decrement, fetchCounter, fetchCounterPut, increment, subtract} from "../../store/actions";
import "./Counter.css";

const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector(state => state.counter);

  useEffect(() => {
    dispatch(fetchCounter());
  }, [dispatch]);

  useEffect(() => {
    if (!!counter) {
      dispatch(fetchCounterPut(counter));
    }
  }, [dispatch, counter]);

  const increaseCounter = () => dispatch(increment());
  const decreaseCounter = () => dispatch(decrement());
  const plusCounter = () => dispatch(add(5));
  const minusCounter = () => dispatch(subtract(5));


  return (
      <div className="Counter">
        <h1>{counter}</h1>
        <button onClick={increaseCounter}>Increase</button>
        <button onClick={decreaseCounter}>Decrease</button>
        <button onClick={plusCounter}>Increase by 5</button>
        <button onClick={minusCounter}>Decrease by 5</button>
      </div>
  );
};

export default Counter;