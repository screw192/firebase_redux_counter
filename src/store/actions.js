import axios from "axios";

export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT"
export const ADD = "ADD";
export const SUBTRACT = "SUBTRACT";

export const FETCH_COUNTER_REQUEST = "FETCH_COUNTER_REQUEST";
export const FETCH_COUNTER_SUCCESS = "FETCH_COUNTER_SUCCESS";
export const FETCH_COUNTER_FAILURE = "FETCH_COUNTER_FAILURE";

export const FETCH_COUNTER_PUT_REQUEST = "FETCH_COUNTER_PUT_REQUEST";
export const FETCH_COUNTER_PUT_SUCCESS = "FETCH_COUNTER_PUT_SUCCESS";
export const FETCH_COUNTER_PUT_FAILURE = "FETCH_COUNTER_PUT_FAILURE";

export const increment = () => ({type: INCREMENT});
export const decrement = () => ({type: DECREMENT});
export const add = value => ({type: ADD, value});
export const subtract = value => ({type: SUBTRACT, value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const fetchCounter = () => {
  return async dispatch => {
    dispatch(fetchCounterRequest());

    try {
      const response = await axios.get("https://js9-burger-screw192-default-rtdb.firebaseio.com/counter.json");
      dispatch(fetchCounterSuccess(response.data));
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const fetchCounterPutRequest = () => ({type: FETCH_COUNTER_PUT_REQUEST});
export const fetchCounterPutSuccess = () => ({type: FETCH_COUNTER_PUT_SUCCESS});
export const fetchCounterPutFailure = () => ({type: FETCH_COUNTER_PUT_FAILURE});

export const fetchCounterPut = currentCounter => {
  return async dispatch => {
    dispatch(fetchCounterPutRequest());

    try {
      await axios.put("https://js9-burger-screw192-default-rtdb.firebaseio.com/counter.json", currentCounter);
      dispatch(fetchCounterPutSuccess());
    } catch (e) {
      dispatch(fetchCounterPutFailure());
    }
  };
};